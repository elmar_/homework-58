import React from 'react';
import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";

const Modal = props => {
    return (
        <>
            <Backdrop closeModal={props.close} backdrop={props.back} />
            <div className="Modal" style={{display: props.modal ? "block" : "none"}}>
                <h3 className="Modal-title"><span>{props.title}</span> <button type="button" onClick={props.close}>X</button></h3>
                <p>{props.children}</p>
            </div>
        </>
    );
};

export default Modal;