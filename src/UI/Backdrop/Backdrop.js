import React from 'react';
import './Backdrop.css';

const Backdrop = props => {
    return (
        <div className="Backdrop" style={{display: props.backdrop ? "block" : "none"}} onClick={props.closeModal}>
        </div>
    );
};

export default Backdrop;