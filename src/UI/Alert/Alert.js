import React from 'react';
import './Alert.css';

const Alert = props => {
    let alertType;
    switch (props.alert[1]) {
        case 'warning':
            alertType = (
                <div className="Alert warning">
                    <p>{props.children}</p>
                    <button onClick={props.close}>close</button>
                </div>
            );
            break;
        case 'success':
            alertType = (
                <div className="Alert success">
                    <p>{props.children}</p>
                </div>
            );
            break;
        default:
    }
    return (
        <>
            {alertType}
        </>
    );
};

export default Alert;