import React, {useState} from 'react';
import './App.css';
import Modal from "./UI/Modal/Modal";
import Alert from "./UI/Alert/Alert";

const App = () => {
    const [modalON, setModalOn] = useState(false);
    const [alertOn, setAlertON] = useState([false, ""]);
    const [backdropON, setBackdropON] = useState(false);

    const showModal = () => {
      setModalOn(true);
      setBackdropON(true);
    };

    const closeModal = () => {
      setModalOn(false);
      setBackdropON(false);
    };

    const showAlert = type => {
        setAlertON([true, type]);
    };

    const closeAlert = () => {
      setAlertON(false);
    };

    return (
        <div className="App">
            <Modal title={'title text'} close={closeModal} modal={modalON} back={backdropON}>Some text</Modal>
            <p><button type="button" onClick={showModal}>Модальное окно</button></p>
            <p><button type="button" onClick={() => showAlert("warning")}>Алерт warning</button></p>
            <p><button type="button" onClick={() => showAlert("success")}>Алерт success</button></p>
            <Alert close={closeAlert} showAlert={showAlert} alert={alertOn}>This is alert {alertOn[1]}</Alert>
        </div>
    );
};

export default App;